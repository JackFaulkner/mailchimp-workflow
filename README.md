# Gulp Mailchimp Workflow 

This is a Gulp-powered workflow for building HTML email templates for Mailchimp. It does the following things for you:

* Builds an HTML file from [Mustache](https://mustache.github.io/mustache.5.html) partials.
* Compiles your SASS to CSS.
* Inlines the CSS into HTML elements' `style` attributes.
* Adds an optional background image via CSS and VML (for Outlook).
* Compresses images and copies all assets to the dist folder.
* Zips up your template so it's ready for uploading to Mailchimp.

It also contains some handy article layouts based on Mailchimp's [email blueprints](https://github.com/mailchimp/email-blueprints).

## Quick start

- Run `gulp`.
- Your template will appear in the dist folder.

## How to use

#### Installation

* Download or clone the repo.
* Run `npm install` to install the dependencies.

#### Configuration


* Edit the SASS files in the sass/theme folder. Styles in mobile.scss will not be inlined but remain in the <head> so media queries work on mobile devices.
* Adjust settings for your background image  

```json
{
  "background": {
    "show": true,
    "color": "#FFFFFF",
    "image": "background.jpg"
  }
}
```

#### Use
 
 * Develop your Mailchimp Template in the `src` folder
 * Run `gulp`

 Watch your `dist` folder. What you get is one HTML file with inlined CSS rules and all your assets. In addition you get a ready to use `template.zip` you can upload to Mailchimp.
 
#### Warnings

Be careful not to use the same CSS selector more than once, as this can cause issues when the styles are inlined. You can repeat selectors in the mobile stylesheet as these are inserted in a separate `<style>` tag.


## Credits
 
Big thanks to [Gulp Mailchimp Template](https://github.com/whatwedo/gulp-mailchimp-template) which provided the core functionality.
