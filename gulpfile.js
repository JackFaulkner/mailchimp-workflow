'use strict';

var gulp = require( 'gulp' );
var clean = require( 'gulp-htmlclean' );
var del = require('del');
var imagemin = require('gulp-imagemin');
var inline = require( 'gulp-inline-css' );
var lazypipe = require( 'lazypipe' );
var livereload = require( 'gulp-livereload' );
var mustache = require("gulp-mustache");
var notify = require('gulp-notify');
var sass = require( 'gulp-sass' );
var size = require('gulp-size');
var watch = require( 'gulp-watch' );
var zip = require( 'gulp-zip' );

var config = require('./config.json');

var compileCSS = lazypipe()
    .pipe( function( opt, cb ) {
        return sass( opt ).on('error', cb);
    }, {}, sass.logError )
    .pipe( gulp.dest, './src/css' );

var buildHTML = lazypipe()
  	.pipe( mustache, {
  		background: {
    		show: config.background.show,
    		color: config.background.color,
    		image: config.background.image
    	}
  	}, { extension: '.html' } )
    .pipe( inline, { applyLinkTags: false, removeStyleTags: false, removeLinkTags: false, url: ' ' } )
    .pipe( size, { showFiles: true, showTotal: false } )
    .pipe( gulp.dest, './dist' );
    
var processImages = lazypipe()
	  .pipe( imagemin, { optimizationLevel: 5 } )
  	.pipe( size )
    .pipe( gulp.dest, './dist/images' );
    

// Compile Sass
gulp.task('sass', function() {
	return gulp.src( './src/sass/**/*.scss' ).pipe( compileCSS() );
});

// Build HTML
gulp.task( 'html', ['sass'], function() {
	return gulp.src( './src/*.mustache' ).pipe( buildHTML() );
});

// Copy and minify assets
gulp.task( 'images', function() {
  	return gulp.src( './src/images/**/*' ).pipe( processImages() );
});

// Clean dist folder
gulp.task( 'clean', function() {
		del( './dist/*' );
});

// Zip assets
gulp.task( 'zip', [ 'html', 'images' ], function() {
    return gulp.src( './dist/**/*' )
      	.pipe( zip( 'template.zip' ) )
				.pipe( gulp.dest( './dist' ) );
});

// Watch
gulp.task( 'watch', function() {
	return watch( [ './src/sass/**/*.scss', './src/**/*.mustache' ], function(){
		gulp.src( './src/sass/**/*.scss' ).pipe( compileCSS() ).on( 'end', function(){
			gulp.src( './src/*.mustache' ).pipe( buildHTML() );
		});
	})
});

// Dist
gulp.task( 'default', [ 'zip' ] );
